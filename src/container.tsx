import React, { Dispatch, useMemo } from "react";
import * as ReactLeaflet from "react-leaflet";
import MapEvents, { OnClick } from "./events";
import { TileLayer, Props as TileLayerProps } from "./tiles";
import L, { LatLng } from "leaflet";

export type MapContainerProps =
    ReactLeaflet.MapContainerProps &
    OnClick &
    Pick<TileLayerProps, 'map'> &
    Pick<TileLayerProps, 'edgeBufferTiles'> &
    {
        setCenter: Dispatch<LatLng>
        setZoom: Dispatch<number>
        tolerance?: number
        padding?: number
    }

export default function MapContainer(
    {
        map = 'alidade_smooth_dark',
        edgeBufferTiles,
        setCenter,
        setZoom,
        tolerance = 12,
        padding = 0.5,
        onClick,
        children,
        ...mapProps
    }: MapContainerProps
) {
    const canvas = useMemo(() => L.canvas({ tolerance, padding, }), [ tolerance, padding, ])
    return <ReactLeaflet.MapContainer {...mapProps} renderer={canvas}>
        <MapEvents setCenter={setCenter} setZoom={setZoom} onClick={onClick}>
            <TileLayer map={map} edgeBufferTiles={edgeBufferTiles} />
            {children}
        </MapEvents>
    </ReactLeaflet.MapContainer>
}
