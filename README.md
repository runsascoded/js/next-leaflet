# @rdub/next-leaflet
[Leaflet] utils for [Next.js]

<a href="https://npmjs.org/package/@rdub/next-leaflet" title="View @rdub/next-leaflet on NPM"><img src="https://img.shields.io/npm/v/@rdub/next-leaflet.svg" alt="@rdub/next-leaflet NPM version" /></a>

## Install
```bash
npm install @rdub/next-leaflet
```

You'll also want to this CSS (required by [src/map/settings.tsx](src/map/settings.tsx)) to your `_app.tsx`:
```typescript
import '@fortawesome/fontawesome-svg-core/styles.css'
```

Note also how [src/map/settings.tsx](src/map/settings.tsx#L6-L7) disables automatic fontawesome CSS injection:
```typescript
import { config, IconProp } from '@fortawesome/fontawesome-svg-core'
config.autoAddCss = false
```

Used in:
- [ctbk.dev](https://ctbk.dev)
- [crashes.hudcostreets.org](https://crashes.hudcostreets.org)

[Leaflet]: https://leafletjs.com/
[Next.js]: https://nextjs.org/
